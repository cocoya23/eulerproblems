/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.doctordice.eulerproject;

import java.math.BigInteger;

/**
 *
 * @author enrique
 */
public class P016 {

    
    public static void main(String[] args) {
        BigInteger val=new BigInteger("2");
        for (long i = 1; i < 1000; i++) {
            val = val.multiply(new BigInteger("2"));
        }
        System.out.println(val);
        char[] charArray = (""+val).toCharArray();
        BigInteger sum = new BigInteger("0");
        for (char d : charArray) {
            sum = sum.add(new BigInteger(d+""));
        }
        System.out.println(sum);
    }
    
}
