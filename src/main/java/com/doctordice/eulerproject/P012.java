
package com.doctordice.eulerproject;


public class P012 {

    
    public static void main(String[] args) {
        for (int i = 1; i <= 1000000; i++) {
            long sum = 0;
            for (int j = 1; j <= i; j++) {
                sum+=j;
            }
            int divisors = 0;
            for (int j = 1; j <= sum; j++) {
                if(sum%j==0){
                    divisors++;
                }
            }
            System.out.println(i+":"+sum+"="+divisors);
            if(divisors>500){
                System.out.println(sum);
                break;
            }
        }
    }
    
}
