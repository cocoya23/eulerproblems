package com.doctordice.eulerproject;

import java.text.SimpleDateFormat;
import java.util.Calendar;


public class P019 {

    
    public static void main(String[] args) {
        
        Calendar init = Calendar.getInstance();
        init.set(1901, 0, 1);
        Calendar finish = Calendar.getInstance();
        finish.set(2000,11,31);
        
        SimpleDateFormat format = new SimpleDateFormat("dd-MMMM-yyyy");
        
        System.out.println(format.format(init.getTime()));
        System.out.println(format.format(finish.getTime()));
        
        int domingos=0;
        while (init.getTimeInMillis()<=finish.getTimeInMillis()) {
            
            if(init.get(Calendar.DAY_OF_WEEK)==Calendar.SUNDAY){
                domingos++;
            }
            init.add(Calendar.MONTH, 1);
            System.out.println(format.format(init.getTime()));
        }
        
        System.out.println("domingos:"+domingos);
        
    }
    
}
