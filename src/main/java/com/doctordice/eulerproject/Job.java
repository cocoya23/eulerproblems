package com.doctordice.eulerproject;

public class Job implements Runnable{
    
    private JobListener jobListener;
    
    public interface JobListener{
        
        public void acabo(String resultado);
        public void vuelta(int ciclo);
        
    }

    @Override
    public void run() {
        for (int i = 1; i <= 10; i++) {
            if(jobListener!=null)
                jobListener.vuelta(i);
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                
            }
        }
        
        if(jobListener!=null)
            jobListener.acabo("ya acabe y por aqui puedo regresar lo que quiera");
        
    }

    public void addJobListener(JobListener jobListener) {
        this.jobListener = jobListener;
    }

}
