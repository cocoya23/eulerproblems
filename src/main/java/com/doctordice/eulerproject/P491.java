package com.doctordice.eulerproject;

import java.math.BigInteger;

public class P491 {

    
    public static void main(String[] args) {
        
        BigInteger num = new BigInteger("10012233445566778899");
        BigInteger divisor = new BigInteger("11");
        BigInteger limite = new BigInteger("99887766554433221100");
        
        int sum = 0;
        while (num.compareTo(limite)<0) {
            if(isPandigital(num.toString())){
                if(num.mod(divisor).equals(BigInteger.ZERO)){
                    System.out.println(num.toString());
                    sum++;
                }
            }
            num=num.add(BigInteger.ONE);
        }
        System.out.println(sum);
        
    }
    
    public static boolean isPandigital(String cadena){
        
        for (int i = 0; i < 10; i++) {
            
            char[] arreglo = cadena.toCharArray();
            
            int concurrencia = 0;
            for (char b : arreglo) {
                if((b+"").equals(i+"")){
                    concurrencia ++;
                }
            }            
            
            if(concurrencia!=2)return false;
        }
        return true;
    }
    
}
