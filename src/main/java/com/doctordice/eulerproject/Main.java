package com.doctordice.eulerproject;

public class Main implements Job.JobListener{
    
    public Main(){
        
        Job job = new Job();
        job.addJobListener(this);
        Thread h = new Thread(job);
        h.start();
        
        System.out.println("ya ejecute el hilo");
        
    }
    
    
    public static void main(String[] args) {
        
        new Main();
        
    }

    @Override
    public void acabo(String resultado) {
        System.out.println("ya acabo el hilo y su resultado fue:");
        System.out.println(resultado);
    }

    @Override
    public void vuelta(int ciclo) {
        System.out.println("va en la vuelta: "+ciclo);
    }

}
