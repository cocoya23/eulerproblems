package com.doctordice.eulerproject;

public class P014 {

    public static void main(String[] args) {

        long maxOrbits = 0;
        long number = 0;
        for (long i = 1; i <= 1000000; i++) {
            long orbitsNumer = CollatzConjeture.getCollatzOrbitSize(i, 1);
            if (orbitsNumer > maxOrbits) {
                maxOrbits = orbitsNumer;
                number = i;
            }
        }
        System.out.println(number + "=>" + maxOrbits);
    }

    public static class CollatzConjeture {

        public static String getCollatzSequence(long n, int limit) {
            StringBuilder sb = new StringBuilder();
            sb.append(n);
            while (n > limit) {
                n = collatz(n);
                sb.append(" -> ").append(n);
            }
            return sb.toString();
        }

        public static long getCollatzOrbitSize(long n, int limit) {
            int numeros = 1;
            while (n > limit) {
                numeros++;
                n = collatz(n);
            }
            return numeros;
        }

        public static boolean collatzOrbitlimitEqualsEnd(long n, int limit) {
            while (n > limit) {
                n = collatz(n);
            }
            return n == limit;
        }

        public static long collatz(long n) {
            if (n % 2 == 0) {
                n /= 2;
            } else {
                n = (3 * n) + 1;
            }
            return n;
        }
    }
}
