package com.doctordice.eulerproject;

public class P021 {

    public static void main(String[] args) {
        int sum = 0;
        for (int i = 10000; i > 0; i--) {
            if(isAmicable(i)){
                sum+=i;
            }
        }
        System.out.println("Suma:"+sum);
        
    }
    
    public static boolean isAmicable(int numero){
        
        int firstResult = sumDivisors(numero);
        int secondResult = sumDivisors(firstResult);
        
         return (numero==secondResult && numero != firstResult);
        
    }
    
    public static int sumDivisors(int numero){
        int sum = 0;
        for (int i = numero-1; i > 0; i--) {
            if((numero%i)==0){
                sum += i;
            }
        }
        return sum;
    }
    
}
