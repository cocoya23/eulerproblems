
package com.doctordice.eulerproject;


public class P005 {

    
    public static void main(String[] args) {
        
        int numero = 0;
        boolean noMultiplo = true;
        while (noMultiplo) {
            numero++;
            boolean entro=false;
            for (int i = 1; i <= 20; i++) {
                
                if(numero%i!=0){
                    entro = true;
                }
                
            }
            if(!entro){
                noMultiplo=false;
            }
            
        }
        System.out.println(numero);
        
    }
    
}
