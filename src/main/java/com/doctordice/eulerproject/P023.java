
package com.doctordice.eulerproject;


public class P023 {

    
    public static void main(String[] args) {
        int sum = 0;
        for (int i = 1; i <= 28123; i++) {
            
            if(isDoubleOfAnAbundant(i)){
                sum += i;
            }
            
        }
        System.out.println("Suma:"+sum);
    }
    
    public static int sumDivisors(int numero){
        int sum = 0;
        for (int i = numero-1; i > 0; i--) {
            if((numero%i)==0){
                sum += i;
            }
        }
        return sum;
    }
    
    public static boolean isAbundant(int numero){
        int sumDivisors = sumDivisors(numero);
        
        return sumDivisors>numero;
    }
    
    public static boolean isDoubleOfAnAbundant(int numero){
        
        if(numero%2==0){
            return isAbundant(numero/2);
        }else{
            return false;
        }
        
    }
    
}
