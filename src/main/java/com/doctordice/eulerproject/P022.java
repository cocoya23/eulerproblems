package com.doctordice.eulerproject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;

public class P022 {

    public static void main(String[] args) throws IOException, URISyntaxException {
        
        String[] names = getNamesFromResource();
        
        Arrays.sort(names);
        int sumScores=0;
        for (int i = 0; i < names.length; i++) {
            String name = names[i];
            int position = (i+1);
            char[] letters = name.toCharArray();
            int sumLetters = 0;
            for (char letter : letters) {
                sumLetters += ((int)letter)-64;
            }
            int score=position*sumLetters;
            sumScores += score;
            System.out.println(name+":"+score);
        }
        System.out.println("Total:"+sumScores);
    }
    
    public static String[] getNamesFromResource() throws IOException, URISyntaxException{
        
        URL url = P022.class.getResource("/files/names.txt");
        
        InputStream is = new FileInputStream(new File(url.toURI()));
        
        byte[] datos = new byte[is.available()];
        
        is.read(datos);
        
        is.close();
        
        String cadena = new String(datos).replaceAll("\"", "");
        
        return cadena.split(",");
        
    }
    
}
