package com.doctordice.eulerproject;

import java.math.BigDecimal;


public class P020 {

    
    public static void main(String[] args) {
        
        BigDecimal producto=new BigDecimal(1);
        for (int i = 100; i > 0; i--) {
            producto = producto.multiply(new BigDecimal(i));
        }
        System.out.println("Producto:"+producto.toPlainString());
        char[] digitos = producto.toPlainString().toCharArray();
        int suma = 0;
        for (char digito : digitos) {
            suma += Integer.parseInt(digito+"");
        }
        System.out.println("Suma:"+suma);
    }
    
}
