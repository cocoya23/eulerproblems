
package com.doctordice.eulerproject;


public class P004 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        for (int i = 100; i <= 999; i++) {
            for (int j = 100; j <= 999; j++) {
                String result = (i*j)+"";
                String inverted = invert(result);
                if(result.equals(inverted)){
                    System.out.println(i+"x"+j+"="+result);
                }
            }
        }
        
    }
    
    private static String invert(String cadena){
        
        char[] chars = cadena.toCharArray();
        
        StringBuilder builder = new StringBuilder();
        for (int i = (chars.length-1); i >= 0; i--) {
            builder.append(chars[i]);
        }
        
        return builder.toString();
        
    }
    
}
