
package com.doctordice.eulerproject;

import java.util.ArrayList;
import java.util.List;

public class P003 {

    
    public static void main(String[] args) {
        List<Long> factores = new ArrayList<Long>();
        long num = 600851475143l;
        for (long i = 2l; i <= num; i++) {
            if(num%i==0){
                boolean wasDivisible = false;
                for (Long factor : factores) {
                    if(i%factor==0){
                        wasDivisible = true;
                    }
                }
                if(!wasDivisible){
                    System.out.println(i);
                    factores.add(i);
                }
            }
            
        }
    }
    
}
