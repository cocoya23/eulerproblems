package com.doctordice.eulerproject;

import java.util.ArrayList;
import java.util.List;

public class P007 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List<Integer> primos = new ArrayList<>();
        int num = 0;
        while(primos.size()<10001) {
            if(isPrime(num)){
                primos.add(num);
            }
            num++;
        }
        System.out.println(primos.get(10000));
    }

    public static boolean isPrime(int n) {
        if (n % 2 == 0) {
            return false;
        }
        for (int i = 3; i * i <= n; i += 2) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

}
