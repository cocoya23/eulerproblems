/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.doctordice.eulerproject;

/**
 *
 * @author enrique
 */
public class P010 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        long sum=0;
        for (long i = 1; i < 2000000; i++) {
            if(isPrime(i)){
                sum+=i;
            }
        }
        //+1 para la pagina
        System.out.println(sum);
    }
    
    public static boolean isPrime(long n) {
        if (n % 2 == 0) {
            return false;
        }
        for (long i = 3; i * i <= n; i += 2) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
    
}
