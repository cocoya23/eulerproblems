package com.doctordice.eulerproject;

public class P002 {

    
    public static void main(String[] args) {
        
        int ant = 1;
        int act = 1;
        int sum = 0;
        int total = 0;
        while (sum < 4000000) {
            System.out.println(act);
            if(act%2==0)total += act;
            sum = ant+act;
            ant = act;
            act = sum;
        }
        System.out.println("Total:"+total);
        
    }
    
    
}
