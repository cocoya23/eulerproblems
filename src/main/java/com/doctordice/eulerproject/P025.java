
package com.doctordice.eulerproject;

import java.math.BigInteger;


public class P025 {
    
    public static void main(String[] args) {
        
        BigInteger ant = new BigInteger("1");
        BigInteger act = new BigInteger("1");
        BigInteger sum = new BigInteger("0");
        BigInteger index = new BigInteger("0");
        
        while (sum.toString().length() < 1000) {
            System.out.println(act);
            sum = ant.add(act);
            ant = act;
            act = sum;
            index.add(new BigInteger("1"));
        }
        System.out.println("---------");        
        System.out.println(index.toString());
        
    }
    
}
